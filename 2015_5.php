<?php

$strings = file('2015_5.txt');

function ruleOne($string)
{
	return (preg_match_all('/[aeiou]/i', $string) >= 3);
}

function ruleTwo($string) {
	
	for($i = 1; $i < strlen($string); $i++)
	{
        if(substr($string, $i, 1) == substr($string, $i - 1, 1)) 
        {
            return true;
        }
	}
	return false;
}

function ruleThree($string)
{
	return !preg_match('/ab|cd|pq|xy/', $string);
}

function isNice($string)
{
    return ruleOne($string) && ruleTwo($string) && ruleThree($string);
}


$nice = 0;
foreach($strings as $key => $str)
{
    if(isNice($str)) 
    {
        $nice++;
    }
}

echo $nice . ' strings are nice with these rules.';

?>